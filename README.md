
This test project shows that lock on internal empty Object works.

In DelayedResource.java:
When synchronized (lock)  is used, threads have to wait before they can execute stuff, so 4 messages will be printed with significant delay between them.
If synchronized (lock)  is commented out,  2 messages will be printed, then significant delay, then 2 more messages.
