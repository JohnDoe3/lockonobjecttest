import java.nio.charset.StandardCharsets;

public class DelayedResource {

	String info = "Information";
	
	Object lock = new Object();
	
	public String getInfoAsString() {
		//comment out synchronized to see how threads will execute this almost at the same time
		synchronized (lock) 
		{
			try {
				Thread.sleep(5000);
			}
			catch (Exception e) {
				System.out.println("Exception caught:"+e);
			}
			return info;
		}
	}
	
	public byte[] getInfoAsUtfBytes() {
		synchronized (lock) 
		{
			try {
				Thread.sleep(5000);
			}
			catch (Exception e) {
				System.out.println("Exception caught:"+e);
			}
			return info.getBytes(StandardCharsets.UTF_8);			
		}
	}
	
}
