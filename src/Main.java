
public class Main {
	
	public static void main(String[] args) {
		DelayedResource resources = new DelayedResource();
		
		Interrogator inter1 = new Interrogator(resources, "inter1");
		Thread thread1 = new Thread(inter1);
		

		Interrogator inter2 = new Interrogator(resources, "inter2");
		Thread thread2 = new Thread(inter2);
		
		thread1.start();
		thread2.start();
		
		try {
			thread1.join(60000);
			thread2.join(60000);
		}
		catch (Exception e) {
			System.out.println("Exception caught!"+e);
		}
		System.out.println();
		System.out.println("Done");
	}
	
}
