import java.util.Arrays;

public class Interrogator implements Runnable {

	DelayedResource resource;
	String name;
	
	public Interrogator(DelayedResource resource_inp, String name_inp) {
		resource = resource_inp;
		name = name_inp;
	}

	@Override
	public void run() {
		String info = resource.getInfoAsString();
		System.out.println(name+" aquiered info:"+info);
		byte[] infoAsBytes = resource.getInfoAsUtfBytes();
		System.out.println(name+" aquiered info as bytes:" + Arrays.toString(infoAsBytes));
	}	
	
	
}
